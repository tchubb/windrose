from distutils.core import setup

setup(
    name='windrose',
    version='1.4',
    author='Lionel Roubeyrie',
    author_email='',
    packages=['windrose'],
    scripts=[],
    url='None',
    license='LICENSE.txt',
    description='Plots Windroses',
    long_description=open('README.txt').read(),
    requires=[
        "numpy",
        "matplotlib",
	"pylab",
    ],
)
